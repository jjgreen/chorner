/*
  tests_helper.h

  Copyright (c) J.J. Green 2016
*/

#ifndef TESTS_HELPER_H
#define TESTS_HELPER_H

#define RELATIVE_ERROR_UNIT 0x1p-53
#define GAMMA(n, u) (((n) * (u))/(1.0 - ((n)*(u))))

#endif
