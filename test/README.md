Unit tests
==========

These tests use the
[CUnit](http://cunit.sourceforge.net/),
[GMP](https://gmplib.org/) and
[MPC](http://www.multiprecision.org/) libraries; on Debian-based
systems install the `libcunit1-dev`, `libgmp3-dev` and `libmpc-dev`
packages.
