/*
  tests_cchorner.h

  Copyright (c) J.J. Green 2016
*/

#include <math.h>
#include <complex.h>
#include <mpc.h>
#include <chorner.h>

#include "tests_cchorner.h"
#include "tests_helper.h"

CU_TestInfo tests_cchorner[] =
  {
    {"empty", test_cchorner_empty},
    {"constant", test_cchorner_constant},
    {"linear1", test_cchorner_linear1},
    {"linear2", test_cchorner_linear2},
    {"theorem 4.5 (figure 1)", test_cchorner_figure1},
    CU_TEST_INFO_NULL,
  };

/* edge-cases */

extern void test_cchorner_empty(void)
{
  double complex px = cchorner(NULL, 0, 1);

  CU_ASSERT_DOUBLE_EQUAL(creal(px), 0, 1e-10);
  CU_ASSERT_DOUBLE_EQUAL(cimag(px), 0, 1e-10);
}

extern void test_cchorner_constant(void)
{
  double complex p[1] = {1}, x = 3;
  double complex px = cchorner(p, 1, x);

  CU_ASSERT_DOUBLE_EQUAL(creal(px), 1, 1e-10);
  CU_ASSERT_DOUBLE_EQUAL(cimag(px), 0, 1e-10);
}

extern void test_cchorner_linear1(void)
{
  double complex p[2] = {1, 1}, x = 1 + I;
  double complex px = cchorner(p, 2, x);

  CU_ASSERT_DOUBLE_EQUAL(creal(px), 2, 1e-10);
  CU_ASSERT_DOUBLE_EQUAL(cimag(px), 1, 1e-10);
}

extern void test_cchorner_linear2(void)
{
  double complex p[2] = {1, I}, x = 2;
  double complex px = cchorner(p, 2, x);

  CU_ASSERT_DOUBLE_EQUAL(creal(px), 1, 1e-10);
  CU_ASSERT_DOUBLE_EQUAL(cimag(px), 2, 1e-10);
}

#define RELATIVE_ERROR_UNIT 0x1p-53
#define GAMMA(n, u) (((n) * (u))/(1.0 - ((n)*(u))))

/*
  when passed a polynomial p(x) order n-1, modifies in place
  to produce the polynomial (x-(1+i))p(x), the caller should
  ensure that the array has enough room to hold the result
  since this is not checked here
*/

static void next_poly(double complex *p, size_t n)
{
  double complex q[n];

  memcpy(q, p, n * sizeof(double complex));
  p[n] = 0.0;

  for (size_t i = 0 ; i < n ; i++) p[i] = q[i] * (-1 - I);
  for (size_t i = 0 ; i < n ; i++) p[i+1] += q[i];
}

/*
  Test based on the bound of Theorem 4.5 of [1], as illustrated
  in Figure 1 of that paper.

  [1] Stef Graillat, Valérie Ménissier-Morain, Compensated Horner
  scheme in complex floating point arithmetic, Proceedings of the
  8th Conference on Real Numbers and Computers, Santiago de
  Compostela, Spain, July 7-9, 2008, p.133-146
*/

#define PREC 256

extern void test_cchorner_figure1(void)
{
  double
    u = RELATIVE_ERROR_UNIT;
  double complex
    x = 1.333 + 1.333 * I,
    p[43] = {2*I, -2-2*I, 1};

  mpc_t mHc, meps, mP, mres;

  mpc_init2(mHc, PREC);
  mpc_init2(meps, PREC);
  mpc_init2(mP, PREC);
  mpc_init2(mres, PREC);

  for (size_t n = 3; n < 43 ; n++)
    {
      next_poly(p, n);

      double complex Hc = cchorner(p, n+1, x);

      mpc_set_dc(mHc, Hc, MPC_RNDNU);
      mpc_set_dc(meps, x-1-I, MPC_RNDNU);
      mpc_pow_ui(mP, meps, n, MPC_RNDNU);
      mpc_sub(mres, mHc, mP, MPC_RNDNU);

      double
        res = cabs(mpc_get_dc(mres, MPC_RNDNU)),
        P = cabs(mpc_get_dc(mP, MPC_RNDNU)),
        cond = pow(cabs((1+I+x)/(1+I-x)), n),
        bound = fabs(u + pow(GAMMA(2*n, u), 2)*cond);

      CU_ASSERT(res/P < bound);
    }

  mpc_clear(mHc);
  mpc_clear(meps);
  mpc_clear(mP);
  mpc_clear(mres);
}
