/*
  Reproduces Figure 1 of the report RR2005-04, the illustrated
  inequality (from Theorem 6, ibid.) used in our unit tests.
*/

#include <math.h>
#include <stdio.h>
#include <gmp.h>

#include "chorner.h"

#define RELATIVE_ERROR_UNIT 0x1p-53
#define GAMMA(n, u) (((n) * (u))/(1.0 - ((n)*(u))))

/*
  when passed a polynomial p(x) order n-1, modifies in place
  to produce the polynomial (x-1)p(x), the caller should
  ensure that the array has enough room to hold the result
  since this is not checked here
*/

static void next_poly(double *p, size_t n)
{
  for (size_t i = 0 ; i < n ; i++) p[i] *= -1;
  for (size_t i = 0 ; i < n ; i++) p[n-i] -= p[n-i-1];
}

int main(void)
{
  double
    u = RELATIVE_ERROR_UNIT,
    x = 1.333,
    p[43] = {1, -2, 1};

  mpf_t mHc, meps, mP, mres;

  mpf_set_default_prec(256);
  mpf_inits(mHc, meps, mP, mres, NULL);

  for (size_t n = 3; n < 43 ; n++)
    {
      next_poly(p, n);

      double Hc = crhorner(p, n+1, x);

      mpf_set_d(mHc, Hc);
      mpf_set_d(meps, x-1);
      mpf_pow_ui(mP, meps, n);
      mpf_sub(mres, mHc, mP);

      double
        res = fabs(mpf_get_d(mres)),
        P = mpf_get_d(mP),
        cond = pow(fabs((1+x)/(1-x)), n),
        bound = fabs(u + pow(GAMMA(2*n, u), 2)*cond);

      printf("%e %e %e\n", cond, res/P, bound);
    }
}
