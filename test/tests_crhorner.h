/*
  tests_crhorner.h

  Copyright (c) J.J. Green 2016
*/

#ifndef TESTS_CRHORNER_H
#define TESTS_CRHORNER_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_crhorner[];

extern void test_crhorner_empty(void);
extern void test_crhorner_constant(void);
extern void test_crhorner_linear(void);
extern void test_crhorner_figure1(void);
extern void test_crhorner_figure2(void);

#endif
