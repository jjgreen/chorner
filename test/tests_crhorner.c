/*
  tests_crhorner.h

  Copyright (c) J.J. Green 2016
*/

#include <math.h>
#include <gmp.h>
#include <chorner.h>

#include "tests_crhorner.h"
#include "tests_helper.h"

CU_TestInfo tests_crhorner[] =
  {
    {"empty", test_crhorner_empty},
    {"constant", test_crhorner_constant},
    {"linear", test_crhorner_linear},
    {"theorem 5 (figure 1)", test_crhorner_figure1},
    {"theorem 5 (figure 2)", test_crhorner_figure2},
    CU_TEST_INFO_NULL,
  };

/* edge-cases */

extern void test_crhorner_empty(void)
{
  double px = crhorner(NULL, 0, 1);

  CU_ASSERT_DOUBLE_EQUAL(px, 0, 1e-10);
}

extern void test_crhorner_constant(void)
{
  double p[1] = {1}, x = 3;
  double px = crhorner(p, 1, x);

  CU_ASSERT_DOUBLE_EQUAL(px, 1, 1e-10);
}

extern void test_crhorner_linear(void)
{
  double p[2] = {1, 1}, x = 1;
  double px = crhorner(p, 2, x);

  CU_ASSERT_DOUBLE_EQUAL(px, 2, 1e-10);
}

/*
  When passed a polynomial p(x) order n-1, modifies in place
  to produce the polynomial (x-1)p(x), the caller should
  ensure that the array has enough room to hold the result
  since this is not checked here
*/

static void next_poly(double *p, size_t n)
{
  for (size_t i = 0 ; i < n ; i++) p[i] *= -1;
  for (size_t i = 0 ; i < n ; i++) p[n-i] -= p[n-i-1];
}

/*
  This is a test from Theorem 5 of [1], in particular the
  bound of equation (13), as illustrated in Figure 1.

  [1] S. Graillat, Ph. Langlois, N. Louvet, Compensated Horner
  Scheme, report RR2005-04, 2005.
*/

extern void test_crhorner_figure1(void)
{
  double
    u = RELATIVE_ERROR_UNIT,
    x = 1.333,
    p[43] = {1, -2, 1};

  mpf_t mHc, meps, mP, mres;

  mpf_set_default_prec(256);
  mpf_inits(mHc, meps, mP, mres, NULL);

  for (size_t n = 3; n < 43 ; n++)
    {
      next_poly(p, n);

      double Hc = crhorner(p, n+1, x);

      mpf_set_d(mHc, Hc);
      mpf_set_d(meps, x-1);
      mpf_pow_ui(mP, meps, n);
      mpf_sub(mres, mHc, mP);

      double
        res = fabs(mpf_get_d(mres)),
        P = mpf_get_d(mP),
        cond = pow(fabs((1+x)/(1-x)), n),
        bound = fabs(u + pow(GAMMA(2*n, u), 2)*cond);

      CU_ASSERT(res/P < bound);
    }

  mpf_clears(mHc, meps, mP, mres, NULL);
}

/*
  Likewise, this test is taken from Figure 2 of [1]
*/

extern void test_crhorner_figure2(void)
{
  double
    u = RELATIVE_ERROR_UNIT,
    p[6] = {1, -5, 10, -10, 5, -1};

  mpf_t mHc, meps, mP, mres;
  mpf_set_default_prec(256);
  mpf_inits(mHc, meps, mP, mres, NULL);

  for (int n = 1 ; n <= 1000 ; n++)
    {
      double
	x = 1 - n * 1e-5,
	Hc = crhorner(p, 6, x);

      mpf_set_d(mHc, Hc);
      mpf_set_d(meps, 1-x);
      mpf_pow_ui(mP, meps, 5);
      mpf_sub(mres, mHc, mP);

      double
	res = fabs(mpf_get_d(mres)),
	P = mpf_get_d(mP),
	cond = pow(fabs((1+x)/(1-x)), 5),
	bound = P * (u + pow(GAMMA(10, u), 2)*cond);

      CU_ASSERT(res <= bound);
    }

  mpf_clears(mHc, meps, mP, mres, NULL);
}
