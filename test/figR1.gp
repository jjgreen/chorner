set termopt enhanced
set format x "10^{%L}"
set format y "10^{%L}"
set logscale
set xrange [343:1e35]
set yrange [1e-19:1]
set key left top
set xlabel "Condition number"
set ylabel "Relative accuracy"
plot \
  "figR1.dat" using 1:2 with linespoints title "Compensated Horner", \
  "figR1.dat" using 1:3 with lines title "Theorem 6"
