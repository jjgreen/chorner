chorner documentation
--------------------

The docbook source in `chorner.xml` is used to generate the Unix
man-page `chorner.3` and the plain text `chorner.txt`. The tools
`xsltproc` and `lynx` are required to regenerate these files.

The script `chorner-fetch.sh` pulls the latest version of `chorner.c`
and `chorner.h` from the GitHub master to the current directory.
